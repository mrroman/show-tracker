var benc = require('bncode'),
    http = require('http'),
    buffer = require('buffer'),
    urlparse = require('url').parse;

var addr = 'http://ca.isohunt.com/download/262537137/Desperate+Housewives+S08E04.torrent';

function getTorrent(addr, callback) {
    var url = urlparse(addr);
    var decoder = new benc.decoder();

    http.get({
        host: url.hostname,
        port: url.port,
        path: url.pathname + url.search
    }, function(res) {
        res.on('data', function(chunk) {
            decoder.decode(chunk);
        });

        res.on('end', function() {
            callback(decoder.result()[0]);
        });
    });
}

getTorrent(addr, function(torrent) {
    for (var i = 0; i < torrent.info.files.length; i++) {
       console.log(torrent.info.files[i].path.toString());
    }
});

