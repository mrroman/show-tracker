var httpget = require('./httpget.js');

exports.Searcher = function(title) {
    this.title = title;
};

exports.Searcher.prototype = {
    search: function(callback) {
        var $this = this;

        httpget.get(
	        'http://imdbapi.poromenos.org/js/?name=' + escape(this.title), 
            function(data) {
                $this.parseResults(data);
                callback($this);
            });
    },

    parseResults: function(data) {
        var episodes = JSON.parse(data)[this.title].episodes;
        this.seasons = {};
        this.lastSeason = -1;

        for (var i = 0; i < episodes.length; i++) {
            var episode = episodes[i];
            
            this.seasons[episode.season] = this.seasons[episode.season] || [];
            this.seasons[episode.season].push({
                number: episode.number,
                title: episode.name
            });

            if (this.lastSeason < episode.season) {
                this.lastSeason = episode.season;
                this.lastSeasonEpisodes = this.seasons[this.lastSeason];
            }
        }

        for (var k in this.seasons) {
            this.seasons[k].sort(function(a,b) {
                return a.number - b.number;
            });
        }
    }
};
