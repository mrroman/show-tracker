var http = require('http'),
    url = require('url');

exports.get = function(address, callback) {
    var dataBuffer = '';
    var u = url.parse(address);

    http.get({
        host: u.hostname,
        port: u.port,
        path: u.pathname + u.search
    }, function(res) {
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            dataBuffer += chunk;
        });

        res.on('end', function() {
            callback(dataBuffer);
        });
    });
};


