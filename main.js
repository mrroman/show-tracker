var isohunt = require('./isohunt.js'),
    imdb = require('./imdb.js'),
    sprintf = require('sprintf'),
    rl = require('readline'),
    db = require('dirty')('downloads.db');

db.on('load', function() {

    function downloadEpisode(name) {
        if (db.get(name)) {
            console.log(name + ' already downloaded');
            return;        
        }

        db.set(name, '1');
        
        var searcher = new isohunt.Searcher(name);
        searcher.search(function(data) {
            if (data && data.length > 0) {
                console.log(data.shift().url);
            }
        });
    }

    function checkShow(tvshow) {
        var tv = new imdb.Searcher(tvshow);
        tv.search(function(data) {
            for (var i = 0; i < data.lastSeasonEpisodes.length; i++) {
                var episode = data.lastSeasonEpisodes[i];
 
                downloadEpisode(sprintf.sprintf('%s S%02dE%02d', tvshow, data.lastSeason, episode.number));
            }
        });
    }
    var answer = 'Desperate Housewives';
/*
    var ui = rl.createInterface(process.stdin, process.stdout, null);
    ui.question('What show?', function(answer) {
*/        checkShow(answer);
/*        ui.close();
        process.stdin.destroy();
    });*/

});
