var httpget = require('./httpget.js');

exports.Searcher = function(title) {
    this.title = title;
};

exports.Searcher.prototype = {
    search: function(callback) {
        var $this = this;

        httpget.get(
            'http://ca.isohunt.com/js/json.php?ihq=' + escape(this.title) + '&sort=seeds&start=1&rows=10',
            function(data) {
                $this.parseResults(data);
                callback($this.results);
            });
    },

    parseResults: function(data) {
        var parsed = JSON.parse(data);
        console.log(parsed);
        if (parsed.total_results === 0) {
            return;
        }

        var items = parsed.items.list;
        this.results = new Array();

        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            console.log(item);
            if (item.votes >= 0 && this.checkFileNames(item))  {
                this.results.push({
                    url: item.enclosure_url,
                    title: item.title,
                    seeds: item.Seeds
                });
            }
        }
    },

    checkFileNames: function() {
    }
};

